package lection.two.hwa.one;

public class UpDownTransformer extends TextTransformer {
    public UpDownTransformer() {
        super();
    }

    @Override
    public String transform(String text) {
        char[] arr = text.toCharArray();

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < arr.length; i++) {
            if (i % 2 == 0) {
                sb.append(Character.toUpperCase(arr[i]));
            } else {
                sb.append(Character.toLowerCase(arr[i]));
            }
        }
        return sb.toString();
    }
}
