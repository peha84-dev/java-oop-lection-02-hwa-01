package lection.two.hwa.one;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        TextTransformer tt = new TextTransformer();
        InverseTransformer it = new InverseTransformer();
        UpDownTransformer ud = new UpDownTransformer();

        String str = "TextTextTextTextTextTextText";

        System.out.println(str);
        System.out.println(tt.transform(str));
        System.out.println(it.transform(str));
        System.out.println(ud.transform(str));

        File file = new File("./textTextTransformer.txt");
        TextSaver ts = new TextSaver(tt, file);
        ts.saveTextToFile(str);
        file = new File("./textInverseTransformer.txt");
        ts = new TextSaver(it, file);
        ts.saveTextToFile(str);
        file = new File("./textUpDownTransformer.txt");
        ts = new TextSaver(ud, file);
        ts.saveTextToFile(str);
    }
}