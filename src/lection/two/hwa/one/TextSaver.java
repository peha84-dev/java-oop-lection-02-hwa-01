package lection.two.hwa.one;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class TextSaver {
    private TextTransformer transformer;
    private File file;

    public TextSaver(TextTransformer transformer, File file) {
        super();
        this.transformer = transformer;
        this.file = file;
    }

    public TextSaver() {
        super();
    }

    public TextTransformer getTransformer() {
        return transformer;
    }

    public void setTransformer(TextTransformer transformer) {
        this.transformer = transformer;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void saveTextToFile(String text) {
        if (transformer != null && text != null && file != null) {
            String result;
            result = transformer.transform(text);
            try(PrintWriter pw = new PrintWriter(file)){
                pw.println(result);
            }catch (IOException e) {
                System.out.println("Error");
            }
        } else {
            System.out.println("Incorrect data sent");
        }
    }
}
