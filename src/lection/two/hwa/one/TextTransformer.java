package lection.two.hwa.one;

public class TextTransformer {

    public TextTransformer() {
        super();
    }

    public String transform(String text) {
        return text.toUpperCase();
    }

}
