package lection.two.hwa.one;

public class InverseTransformer extends TextTransformer {

    public InverseTransformer() {
        super();
    }

    @Override
    public String transform(String text) {
        StringBuilder sb = new StringBuilder();
        return sb.append(text).reverse().toString();
    }
}
